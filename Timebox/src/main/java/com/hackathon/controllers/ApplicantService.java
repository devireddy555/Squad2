package com.hackathon.controllers;

import java.sql.SQLException;

import com.hackathon.timebox.model.Applicant;

public interface ApplicantService {
	public void insert(Applicant applicant)  throws SQLException;
	public Applicant get(int applicantId) throws SQLException;
	public Applicant update(int applicantId) throws SQLException;
}
