package com.hackathon.controllers;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hackathon.timebox.model.Applicant;

@Controller
@RequestMapping(value="/loanMortage")
public class ApplicantController {
	@Autowired
	ApplicantService service;
	@RequestMapping(value="/applicants", method=RequestMethod.POST,consumes="application/json")
	@ResponseBody
	public ResponseEntity insertApplicant(@RequestBody Applicant applicant){
		try {
			service.insert(applicant);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@RequestMapping(value="/applicants/{id}", method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public ResponseEntity getApplicant(@PathVariable(name="id") int id){
		try {
			return new ResponseEntity(service.get(id),HttpStatus.OK);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
