package com.hackathon.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hackathon.timebox.model.Applicant;

@Repository
public class ApplicantRepoImpl implements ApplicantRepo {
	
	@Autowired
	DataSource ds;
	
	String sqlinsert = "insert into applicant_data (firstName,lastName,address,"
			+ "creditstatus,applicantincome,loanamount,timecreated) "
			+ "values (?,?,?,?,?,?,?)";
	String sqlSelect = "select * from applicant_data where applicantId=?";
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	public void insert(Applicant applicant) throws SQLException {
		Connection con = ds.getConnection();
		java.sql.PreparedStatement ps = con.prepareStatement(sqlinsert);
		ps.setString(1,applicant.getFirstName());
		ps.setString(2, applicant.getLastName());
		ps.setString(3, applicant.getAddress());
		ps.setString(4,"NOT APPROVED");
		ps.setDouble(5, applicant.getApplicantIncome());
		ps.setDouble(6,applicant.getLoanAmount());
		ps.setString(7, sdf.format(new Date()));
		ps.executeUpdate();
	}

	public Applicant getApplicant(int applicantId) throws SQLException {
		Applicant applicant = new Applicant();
		Connection con = ds.getConnection();
		java.sql.PreparedStatement ps = con.prepareStatement(sqlinsert);
		ps.setInt(1,applicantId);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			applicant.setFirstName(rs.getString("firstname"));
			applicant.setLastName(rs.getString("lastname"));
			applicant.setCreditStatus(rs.getString("creditStatus"));
			applicant.setApplicantIncome(rs.getDouble("applicantincome"));
			applicant.setLoadAmount(rs.getDouble("loanamount"));
			applicant.setTimeCreated(rs.getString("timecreated"));
		}
		return applicant;
	}

	public void update(int applicantId, String status) {
		// TODO Auto-generated method stub
		
	}

}
