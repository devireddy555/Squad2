package com.hackathon.controllers;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.timebox.exception.TimeBoxUserNotFoundException;
import com.hackathon.timebox.model.Applicant;

@Service
public class ApplicantServiceImpl implements ApplicantService {
	@Autowired
	ApplicantRepo repo;
	public void insert(Applicant applicant) throws SQLException {
		repo.insert(applicant);
	}
	public Applicant get(int applicantId) throws SQLException {
		
		return repo.getApplicant(applicantId);
	}
	public Applicant update(int applicantId) throws SQLException {
		return null;
	}
	

	}
