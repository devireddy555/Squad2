package com.hackathon.controllers;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.hackathon.timebox.model.Applicant;

@Repository
public interface ApplicantRepo {
	public void insert(Applicant applicant)throws SQLException;
	public Applicant getApplicant(int applicantId) throws SQLException;
	public void update(int applicantId,String status) throws SQLException;

}
