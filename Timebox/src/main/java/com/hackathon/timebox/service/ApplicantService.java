package com.hackathon.timebox.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hackathon.timebox.model.Applicant;

public interface ApplicantService {
     //Comment
	public void insert(Applicant applicant);
	public Applicant getApplicant(int applicantId);
	public void update(int applicantId,String status);
	
}
