package com.hackathon.timebox.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR,reason="Some Exception Contact Support People") 
public class GenericException extends Exception {

}
