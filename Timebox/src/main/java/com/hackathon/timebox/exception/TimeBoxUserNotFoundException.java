package com.hackathon.timebox.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND,reason="Applicant Not Found")
public class TimeBoxUserNotFoundException extends Exception{

}
